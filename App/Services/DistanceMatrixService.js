// @flow

import apisauce from 'apisauce'
import Config from 'react-native-config'

import type { Coordinates } from '../Containers/LaunchScreen'

export type DistanceMatrixServiceType = {
  getDistance: (origins: Array<Coordinates>, destinations: Array<Coordinates>) => Promise<*>,
}

const create = (baseURL: string = Config.GOOGLE_MAPS_API): DistanceMatrixServiceType => {
  const api = apisauce.create({ baseURL, timeout: 60000, withCredentials: true })

  const toCommaSeparated = (arrayOfCoordinates: Array<Coordinates>) => arrayOfCoordinates.map(origin => `${origin.latitude},${origin.longitude}`).join('|')

  const getDistance = (origins: Array<Coordinates>, destinations: Array<Coordinates>) =>
    api.get(`distancematrix/json?origins=${toCommaSeparated(origins)}&destinations=${toCommaSeparated(destinations)}&key=${Config.GOOGLE_API_KEY}`)

  return {
    getDistance,
  }
}

export default {
  create,
}
