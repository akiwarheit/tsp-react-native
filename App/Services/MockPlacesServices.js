const create = () => {
  const getPlaces = (query: string, lat: number, lng: number) => ({
    ok: true,
    data: require('../Fixtures/places.json'),
  })

  return {
    getPlaces,
  }
}

export default {
  create,
}
