// @flow

import apisauce from 'apisauce'
import Config from 'react-native-config'

export type PlacesServiceType = {
  getPlaces: (string, number, number) => Promise<*>,
  getPlace: string => Promise<*>,
}

const create = (baseURL: string = Config.GOOGLE_MAPS_API): PlacesServiceType => {
  const api = apisauce.create({ baseURL, timeout: 60000, withCredentials: true })

  const getPlaces = (query: string, lat: number, lng: number) =>
    api.get(`place/autocomplete/json?input=${query}&location=${lat},${lng}&radius=1500000&strictbounds&key=${Config.GOOGLE_API_KEY}`)

  const getPlace = (placeId: string) => api.get(`place/details/json?placeid=${placeId}&fields=name,geometry,formatted_address&key=${Config.GOOGLE_API_KEY}`)

  return {
    getPlaces,
    getPlace,
  }
}

export default {
  create,
}
