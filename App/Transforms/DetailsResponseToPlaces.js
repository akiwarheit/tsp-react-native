export default function (detailsResponses: Array) => detailsResponses.map(detailsResponse => ({
  ...detailsResponse.data.result
}))
