// @flow

import { takeLatest, all } from 'redux-saga/effects'
import PlacesServices from '../Services/PlacesServices'
import MockPlacesServices from '../Services/MockPlacesServices'
import DistanceMatrixService from '../Services/DistanceMatrixService'
import FixtureAPI from '../Services/FixtureApi'
import DebugConfig from '../Config/DebugConfig'

/* ------------- Types ------------- */

import { StartupTypes } from '../Redux/StartupRedux'
import { PlacesTypes } from '../Redux/PlacesRedux'
import { DistanceMatrixTypes } from '../Redux/DistanceMatrixRedux'

/* ------------- Sagas ------------- */

import { startup } from './StartupSagas'
import { getPlaces } from './PlacesSagas'
import { getDistanceMatrix } from './DistanceMatrixSagas';

/* ------------- API ------------- */

// The API we use is only used from Sagas, so we create it here and pass along
// to the sagas which need it.
const placesService = DebugConfig.useFixtures ? MockPlacesServices.create() : PlacesServices.create()
const distanceMatrixService = DistanceMatrixService.create()

/* ------------- Connect Types To Sagas ------------- */

export default function* root(): Generator<*, *, *> {
  yield all([
    takeLatest(StartupTypes.STARTUP, startup),
    takeLatest(PlacesTypes.PLACES_REQUEST, getPlaces, placesService),
    takeLatest(DistanceMatrixTypes.DISTANCE_MATRIX_REQUEST, getDistanceMatrix, distanceMatrixService),
  ])
}
