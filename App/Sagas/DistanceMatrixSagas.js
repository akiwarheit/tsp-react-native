/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 *************************************************************/

import { call, put } from 'redux-saga/effects'
import DistanceMatrixActions from '../Redux/DistanceMatrixRedux'
import type { DistanceMatrixServiceType } from '../Services/DistanceMatrixService'

import GeneticAlgorithm from '../Lib/GeneticAlgorithm'

export function* getDistanceMatrix(distanceMatrixService: DistanceMatrixServiceType, action: Object): Generator<*, *, *> {
  const { origins, destinations } = action
  // get current data from Store
  // const currentData = yield select(DistanceMatrixSelectors.getData)
  // make the call to the api
  const response = yield call(distanceMatrixService.getDistance, origins, destinations)

  // success?
  if (response.ok) {
    // You might need to change the response here - do this with a 'transform',
    // located in ../Transforms/. Otherwise, just pass the data back from the api.
    const distanceData = response.data
    let nodeDistanceData
    durations = []

    for (let originNodeIndex in distanceData.rows) {
      nodeDistanceData = distanceData.rows[originNodeIndex].elements
      /* $FlowFixMe */
      durations[originNodeIndex] = []
      for (let destinationNodeIndex in nodeDistanceData) {
        /* $FlowFixMe */
        if ((durations[originNodeIndex][destinationNodeIndex] = nodeDistanceData[destinationNodeIndex].duration == undefined)) {
          alert("Error: couldn't get a trip duration from API")
          return
        }
        /* $FlowFixMe */
        durations[originNodeIndex][destinationNodeIndex] = nodeDistanceData[destinationNodeIndex].duration.value
      }
    }

    const population = new GeneticAlgorithm.population()
    population.initialize(durations.length)

    const result = yield GeneticAlgorithm.evolvePopulationAsync(population, update => { console.tron.log(update) })

    if (!result.error) {
      const { chromosome } = result.population.getFittest()
      yield put(DistanceMatrixActions.distanceMatrixSuccess(chromosome))
    } else {
      yield put(DistanceMatrixActions.distanceMatrixFailure())
    }
  } else {
    yield put(DistanceMatrixActions.distanceMatrixFailure())
  }
}
