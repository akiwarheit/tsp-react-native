// @flow

import { put, select } from 'redux-saga/effects'
import { is } from 'ramda'

// process STARTUP actions
export function* startup(action: Object): Generator<*, *, *> {
  /* $FlowFixMe */
  if (__DEV__ && console.tron) {
    // straight-up string logging
    console.tron.log("Hello, I'm an example of how to log via Reactotron.")

    // logging an object for better clarity
    /* $FlowFixMe */
    console.tron.log({
      message: 'pass objects for better logging',
    })

    // fully customized!
    /* $FlowFixMe */
    console.tron.display({
      name: '🔥 IGNITE 🔥',
      preview: 'You should totally expand this',
      value: {
        '💃': 'Welcome to the future!',
        someInlineFunction: () => true,
        someGeneratorFunction: startup,
      },
    })
  }
}
