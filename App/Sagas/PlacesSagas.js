// @flow

/* ***********************************************************
 * A short word on how to use this automagically generated file.
 * We're often asked in the ignite gitter channel how to connect
 * to a to a third party api, so we thought we'd demonstrate - but
 * you should know you can use sagas for other flow control too.
 *
 * Other points:
 *  - You'll need to add this saga to sagas/index.js
 *  - This template uses the api declared in sagas/index.js, so
 *    you'll need to define a constant in that file.
 *************************************************************/

import { call, put, all } from 'redux-saga/effects'
import PlacesActions from '../Redux/PlacesRedux'
import type { PlacesServiceType } from '../Services/PlacesServices'

export function* getPlaces(service: PlacesServiceType, action: Object): Generator<*, *, *> {
  const { query, lat, lng } = action
  const response = yield call(service.getPlaces, query, lat, lng)

  if (response.ok) {
    const details = yield all([...response.data.predictions.map(prediction => call(service.getPlace, prediction.place_id))])
    const transformedDetails = yield details.map((detail: Object, index: number) => ({ ...detail.data.result, description: response.data.predictions[index].description, placeId: response.data.predictions[index].place_id }))
    yield put(PlacesActions.placesSuccess(transformedDetails))
  } else {
    yield put(PlacesActions.placesFailure())
  }
}
