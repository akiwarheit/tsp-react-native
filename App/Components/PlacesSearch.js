// @flow

import React, { Component } from 'react'
import Icon from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity, View, Text, TextInput, FlatList } from 'react-native'
import I18n from 'react-native-i18n'
import styles from './Styles/PlacesSearchStyle'

type Props = {
  onClose: () => void,
  searchForPlaces: string => any,
  places: Array<Object>,
  onPlaceSelected: Object => void,
}

export default class PlacesSearch extends Component<Props, {}> {
  renderPlace = ({ item }: Object) => (
    <TouchableOpacity
      onPress={this.onPress(item)}
    >
      <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'white', padding: 20 }}>
        <Icon name='plus-circle' size={24} color='#7ec0ee' style={{ alignSelf: 'center' }} />
        <Text style={{ color: 'black', fontSize: 16, marginHorizontal: 10, alignSelf: 'center' }}>{item.formatted_address}</Text>
      </View>
    </TouchableOpacity>
  )

  onPress = (item: Object) => () => {
    this.props.onPlaceSelected(item)
    this.props.onClose()
  }

  keyExtractor = (item: Object) => item.placeId

  onChangeText = (text: string) => text && this.props.searchForPlaces(text)

  render() {
    return (
      <View style={styles.container}>
        <TouchableOpacity onPress={this.props.onClose}>
          <View style={{ flexDirection: 'row', justifyContent: 'flex-end', padding: 5 }}>
            <Icon name='times-circle' size={30} color='#cd0000' style={{ alignSelf: 'center' }} />
          </View>
        </TouchableOpacity>
        <View style={{ backgroundColor: 'white' }}>
          <TextInput style={{ height: 40, borderColor: 'gray', fontSize: 16 }} onChangeText={this.onChangeText} />
        </View>
        <View style={{ flex: 1, width: '100%', alignSelf: 'flex-end' }}>
          <FlatList renderItem={this.renderPlace} data={this.props.places} keyExtractor={this.keyExtractor} />
        </View>
      </View>
    )
  }
}
