// @flow

import React, { Component } from 'react'
import { View, Text, ScrollView, TouchableOpacity, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import I18n from 'react-native-i18n'
import Swipeable from 'react-native-swipeable'

import styles from './Styles/DirectionsStyle'
import type { Waypoint } from '../Containers/LaunchScreen'

type Props = {
  waypoints: Array<Waypoint>,
  deleteWaypoint: Waypoint => void,
}

type RowData = {
  item: Waypoint,
  index: number,
}

type State = {
  isSwiping: boolean,
}

export default class Directions extends Component<Props, State> {
  constructor (props: Props) {
    super(props)
    this.state = {
      isSwiping: false
    }
  }

  setSwiping = (isSwiping: boolean) => () => this.setState({ isSwiping })

  renderDirection = ({ item, index }: RowData) => (
    <Swipeable
      leftButtons={[this.renderDeleteItem({ item, index })]}
      onSwipeStart={this.setSwiping(true)}
      onSwipeRelease={this.setSwiping(false)}
    >
      <View style={{ padding: 12, flexDirection: 'row', flex: 1 }}>
        <Text style={{ color: '#7ec0ee', fontSize: 24 }}>{index + 1}</Text>
        <Text style={{ color: 'black', fontSize: 12, marginHorizontal: 8, alignSelf: 'center' }}>{item.address}</Text>
      </View>
    </Swipeable>
  )

  renderDeleteItem = ({ item, index }: RowData) => (
    <TouchableOpacity onPress={() => this.props.deleteWaypoint(item)}>
      <Icon name='minus-circle' size={30} color='#ff7f7f' />
    </TouchableOpacity>
  )

  render() {
    return (
      <View style={styles.container}>
        <FlatList data={this.props.waypoints} renderItem={this.renderDirection} keyExtractor={item => item.id} scrollEnabled={!this.state.isSwiping} />
      </View>
    )
  }
}
