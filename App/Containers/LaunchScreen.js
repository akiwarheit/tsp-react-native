// @flow

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Modal, Button, ScrollView, Text, Image, View, PermissionsAndroid, Platform, TextInput } from 'react-native'
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'
import Config from 'react-native-config'
import I18n from 'react-native-i18n'
import { head, last } from 'lodash'
import { reject, eqProps } from 'ramda'

import PlacesActions, { PlacesSelectors } from '../Redux/PlacesRedux'
import DistanceMatrixActions, { DistanceMatrixSelectors } from '../Redux/DistanceMatrixRedux'
import { Images } from '../Themes'
import PlacesSearch from '../Components/PlacesSearch'

// Styles
import styles from './Styles/LaunchScreenStyles'
import Directions from '../Components/Directions'

export type Coordinates = {
  longitude: number,
  latitude: number,
}

export type Waypoint = {
  ...$Exact<Coordinates>,
  ...$Exact<{ name: string, id: string, address: string }>,
}

type Props = {
  searchForPlaces: (string, number, number) => void,
  resetPlaces: () => void,
  places: Array<Object>,
  fetchDistance: (origins: Array<Coordinates>, destinations: Array<Coordinates>) => void,
  optimalWaypointOrder: Array<number>
}

type State = {
  hasLocationAccess: boolean,
  initialRegion: Coordinates,
  waypoints: Array<Waypoint>,
  showPlacesSearchModal: boolean,
}

const DEFAULT_LOCATION_ACCESS = Platform.OS === 'android' ? false : true

const DEFAULT_INITIAL_REGION = {
  longitude: 174.78333000000003,
  latitude: -36.849995,
  latitudeDelta: 0.09,
  longitudeDelta: 0.09,
}

class LaunchScreen extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = {
      hasLocationAccess: DEFAULT_LOCATION_ACCESS,
      initialRegion: DEFAULT_INITIAL_REGION,
      waypoints: [],
      showPlacesSearchModal: false,
    }
  }

  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION, {
        title: I18n.t('requestLocationPermissionTitle'),
        message: I18n.t('requestLocationPermissionMessage'),
      })
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.setState({
          hasLocationAccess: true,
        })
      } else {
        this.setState({
          hasLocationAccess: false,
        })
      }
    } catch (err) {
      this.setState({
        hasLocationAccess: false,
      })
    }
  }

  onBackPress = () => {
    if (this.state.showPlacesSearchModal) {
      this.togglePlacesSearchModal()
      return true
    }

    return false
  }

  getMarkerOrder = (index: number) => this.props.optimalWaypointOrder[index] >= 0 ? I18n.t('waypointOrder', { waypointOrder: this.props.optimalWaypointOrder[index] + 1 }) : I18n.t('unableToDetermineOrder')

  componentDidMount() {
    Platform.OS === 'android' && this.requestLocationPermission()
    this.state.hasLocationAccess && navigator.geolocation.getCurrentPosition(this.onLocation, this.onLocationError)
    this.state.hasLocationAccess && navigator.geolocation.watchPosition(this.onLocation, this.onLocationError)
  }

  componentDidUpdate (prevProps, prevState) {
    if (prevState.waypoints !== this.state.waypoints) {
      const coordinates = this.state.waypoints.map(waypoint => ({ latitude: waypoint.latitude, longitude: waypoint.longitude }))
      coordinates.length >= 3 && this.props.fetchDistance(coordinates, coordinates)
    }
  }

  onLocation = (location: Object) => {
    const { initialRegion } = this.state
    if (initialRegion.longitude !== location.coords.longitude && initialRegion.latitude !== location.coords.latitude) {
      this.setState({
        initialRegion: {
          ...this.state.initialRegion,
          longitude: location.coords.longitude,
          latitude: location.coords.latitude,
        },
      })
    }
  }

  onLocationError = (error: Object) => {}

  togglePlacesSearchModal = () => {
    this.props.resetPlaces()
    this.setState({
      showPlacesSearchModal: !this.state.showPlacesSearchModal,
    })
  }

  onPlaceSelected = (place: Object) => {
    this.setState({
      waypoints: this.state.waypoints.concat({
        id: place.placeId,
        address: place.formatted_address,
        name: place.name,
        latitude: place.geometry.location.lat,
        longitude: place.geometry.location.lng,
      }),
    })
  }

  getOrigin = () => head(this.state.waypoints)

  getDestination = () => last(this.state.waypoints)

  getWaypoints = () => (this.canDrawWaypoints() ? this.state.waypoints.slice(1, this.state.waypoints.length - 1) : [])

  canDrawWaypoints = () => this.state.waypoints.length >= 2

  shouldRenderHeader = () => this.state.waypoints.length >= 1

  onDeleteWaypoint = (waypoint: Waypoint) => {
    const waypoints = this.state.waypoints.filter(toFilter => toFilter.id !== waypoint.id)
    this.setState({
      waypoints,
    })
  }

  render() {
    const { latitude, longitude } = this.state.initialRegion

    return this.state.hasLocationAccess ? (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <MapView
          style={{
            width: '100%',
            height: '100%',
          }}
          provider={PROVIDER_GOOGLE}
          initialRegion={this.state.initialRegion}
        >
          {this.state.waypoints.map((waypoint, index) => (
            <MapView.Marker coordinate={waypoint} key={index} title={this.getMarkerOrder(index)} description={waypoint.address} />
          ))}
          <MapViewDirections origin={this.getOrigin()} destination={this.getDestination()} waypoints={this.getWaypoints()} apikey={Config.GOOGLE_API_KEY} />
          <MapViewDirections origin={this.getDestination()} destination={this.getOrigin()} apikey={Config.GOOGLE_API_KEY} />
        </MapView>
        <View style={{ position: 'absolute', top: 0, left: 0, flex: 1, backgroundColor: 'white', width: '100%', justifyContent: 'center' }}>
          <Button onPress={this.togglePlacesSearchModal} title={I18n.t('search')} />
        </View>
        <View style={{ position: 'absolute', bottom: 0, left: 0, flex: 1, backgroundColor: 'white', width: '100%' }}>
          {this.shouldRenderHeader() && (
            <View style={{ flex: 1, flexDirection: 'row', backgroundColor: 'white', padding: 20 }}>
              <Text style={{ color: 'black', fontSize: 16, marginHorizontal: 10, alignSelf: 'center' }}>{I18n.t('directions')}</Text>
            </View>
          )}
          <Directions waypoints={this.state.waypoints} deleteWaypoint={this.onDeleteWaypoint} />
        </View>
        <Modal visible={this.state.showPlacesSearchModal} transparent onRequestClose={() => {}} animationType='slide'>
          <PlacesSearch
            onClose={this.togglePlacesSearchModal}
            searchForPlaces={query => this.props.searchForPlaces(query, latitude, longitude)}
            places={this.props.places}
            onPlaceSelected={this.onPlaceSelected}
          />
        </Modal>
      </View>
    ) : (
      <View />
    )
  }
}

const mapStateToProps = (state: Object) => ({
  places: PlacesSelectors.getData(state),
  optimalWaypointOrder: DistanceMatrixSelectors.getData(state),
})

const mapDispatchToProps = (dispatch: Object => {}) => ({
  searchForPlaces: (query: string, lat: number, lng: number) => dispatch(PlacesActions.placesRequest(query, lat, lng)),
  resetPlaces: () => dispatch({ type: 'RESET' }),
  fetchDistance: (origins: Array<Coordinates>, destinations: Array<Coordinates>) => dispatch(DistanceMatrixActions.distanceMatrixRequest(origins, destinations))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(LaunchScreen)
