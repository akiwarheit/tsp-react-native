import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  distanceMatrixRequest: ['origins', 'destinations'],
  distanceMatrixSuccess: ['data'],
  distanceMatrixFailure: null
})

export const DistanceMatrixTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: [],
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const DistanceMatrixSelectors = {
  getData: state => state.distanceMatrix.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state) =>
  state.merge({ fetching: true, payload: null })

// successful api lookup
export const success = (state, action) => {
  const { data } = action
  return state.merge({ fetching: false, error: null, data })
}

// Something went wrong somewhere.
export const failure = state =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.DISTANCE_MATRIX_REQUEST]: request,
  [Types.DISTANCE_MATRIX_SUCCESS]: success,
  [Types.DISTANCE_MATRIX_FAILURE]: failure
})
