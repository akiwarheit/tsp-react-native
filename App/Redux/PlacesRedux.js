// @flow

import { createReducer, createActions } from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const { Types, Creators } = createActions({
  placesRequest: ['query', 'lat', 'lng'],
  placesSuccess: ['data'],
  placesFailure: null
})

export const PlacesTypes = Types
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  data: null,
  fetching: null,
  payload: null,
  error: null
})

/* ------------- Selectors ------------- */

export const PlacesSelectors = {
  getData: (state: Object) => state.places.data
}

/* ------------- Reducers ------------- */

// request the data from an api
export const request = (state: Object) =>
  state.merge({ fetching: true, error: null, payload: null })

// successful api lookup
export const success = (state: Object, action: Object) => {
  const { data } = action
  return state.merge({ fetching: false, error: null, data })
}

// Something went wrong somewhere.
export const failure = (state: Object) =>
  state.merge({ fetching: false, error: true, payload: null })

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.PLACES_REQUEST]: request,
  [Types.PLACES_SUCCESS]: success,
  [Types.PLACES_FAILURE]: failure
})
